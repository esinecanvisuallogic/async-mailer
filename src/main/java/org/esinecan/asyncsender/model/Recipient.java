package org.esinecan.asyncsender.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by eren.sinecan on 01/05/2017.
 */
@AllArgsConstructor
@Getter
public class Recipient {

    private String email;

    private String firstName;

    private String lastName;


    /**
     * Builds Recipient instance from validated input from Scanner.
     * @param validrecipientString String of the form "\"recipient@some-provider.com\";\"firstname0\";\"von lastname0\""
     * @return Recipient object
     */
    public static Recipient build(String validrecipientString){
        String[] recipientBits = validrecipientString.split(";"); //Already validated, safe to assume correct form
        return new Recipient(recipientBits[0], recipientBits[1], recipientBits[2]);
    }

    /**
     * Overriding toString for prettier logs
     *
     * @return String representation of Recipient object
     */
    @Override
    public String toString(){
        return firstName + " " + lastName + " (" + email + ")";
    }
}
