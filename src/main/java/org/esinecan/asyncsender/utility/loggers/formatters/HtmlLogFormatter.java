package org.esinecan.asyncsender.utility.loggers.formatters;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
public class HtmlLogFormatter extends Formatter {

    /**
     * formats log entry into a colored table row
     *
     * @param record log record to be formatted into a colored table
     * @return formatted log entry
     */
    @Override
    public String format(LogRecord record) {
        StringBuffer buf = new StringBuffer(1000);
        buf.append("<tr>\n");

        if (record.getLevel().intValue() == Level.WARNING.intValue()) {
            buf.append("\t<td style=\"color:yellow\">");
            buf.append("<b>");
            buf.append(record.getLevel());
            buf.append("</b>");
        } else if (record.getLevel().intValue() == Level.SEVERE.intValue()) {
            buf.append("\t<td style=\"color:red\">");
            buf.append("<b>");
            buf.append(record.getLevel());
            buf.append("</b>");
        } else if (record.getLevel().intValue() == Level.INFO.intValue()) {
            buf.append("\t<td style=\"color:green\">");
            buf.append("<b>");
            buf.append(record.getLevel());
            buf.append("</b>");
        }else {
            buf.append("\t<td>");
            buf.append(record.getLevel());
        }

        buf.append("</td>\n");
        buf.append("\t<td>");
        buf.append(calcDate(record.getMillis()));
        buf.append("</td>\n");
        buf.append("\t<td>");
        buf.append(formatMessage(record));
        buf.append("</td>\n");
        buf.append("</tr>\n");

        return buf.toString();
    }

    /**
     * returns a formatted date
     *
     * @param millisecs Long dateTime of log entry
     * @return string equivalent of milisecs formatted like: "yyyy.MM.dd - HH:mm:ss"
     */
    private String calcDate(long millisecs) {
        SimpleDateFormat date_format = new SimpleDateFormat("yyyy.MM.dd - HH:mm:ss");
        Date resultdate = new Date(millisecs);
        return date_format.format(resultdate);
    }

    /**
     * Valid xhtml head for html logging
     *
     * @param h handler this header is for.
     * @return
     */
    @Override
    public String getHead(Handler h) {
        return "<!DOCTYPE html>\n<head>\n<style>\n"
                + "table { width: 100% }\n"
                + "th { font:bold 10pt Tahoma; }\n"
                + "td { font:normal 10pt Tahoma; }\n"
                + "h1 {font:normal 11pt Tahoma;}\n"
                + "</style>\n"
                + "</head>\n"
                + "<body>\n"
                + "<h1>" + (new Date()) + "</h1>\n"
                + "<table border=\"0\" cellpadding=\"5\" cellspacing=\"3\">\n"
                + "<tr align=\"left\">\n"
                + "\t<th style=\"width:10%\">Loglevel</th>\n"
                + "\t<th style=\"width:15%\">Time</th>\n"
                + "\t<th style=\"width:75%\">Log Message</th>\n"
                + "</tr>\n";
    }

    /**
     * Valid xhtml tail for html logging
     *
     * @param h handler this tail is for.
     * @return
     */
    @Override
    public String getTail(Handler h) {
        return "</table>\n</body>\n</html>";
    }
}
