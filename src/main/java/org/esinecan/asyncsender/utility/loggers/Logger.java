package org.esinecan.asyncsender.utility.loggers;

import org.esinecan.asyncsender.utility.loggers.formatters.HtmlLogFormatter;
import org.esinecan.asyncsender.model.Recipient;

import java.io.IOException;
import java.util.logging.*;

/**
 * Created by eren.sinecan on 29/04/2017.
 */
public class Logger {

    private static FileHandler fileTxt;
    private static SimpleFormatter formatterTxt;

    private static FileHandler fileHTML;
    private static Formatter formatterHTML;

    private static java.util.logging.Logger logger =
            java.util.logging.Logger.getLogger(java.util.logging.Logger.GLOBAL_LOGGER_NAME);

    /**
     * Logging the successfully sent mail's recipient.
     *
     * @param recipient
     */
    public static void logSuccess(Recipient recipient) {
        logger.info("Mail successfully sent to: " + recipient);
    }

    /**
     * Logging the failed mail's recipient.
     *
     * @param recipient
     */
    public static void logSendMailError(Recipient recipient) {
        logger.warning("Mail could not be sent to: " + recipient);
    }

    /**
     * Logging Validation failure.
     *
     * @param chunk
     */
    public static void logParsingError(String chunk){
        logger.severe("Parsing failed for the chunk: " + chunk);
    }

    /**
     * Logging failure to read recipient file
     *
     * @param filePath
     * @param e
     */
    public static void logFileReadError(String filePath, Exception e){
        logger.severe("File Not Found: \"" + filePath + "\" threw exception: " + e);
    }

    /**
     * We attach handlers for our Html and Text formatters. Text formatter is the default
     * SimpleFormatter, Html has custom org.esinecan.asyncsender.utility.loggers.formatters.HtmlLogFormatter
     *
     * @throws IOException
     */
    public static void setup() throws IOException {

        logger.setLevel(Level.INFO);
        fileTxt = new FileHandler("Logging.txt");
        fileHTML = new FileHandler("Logging.html");

        formatterTxt = new SimpleFormatter();
        fileTxt.setFormatter(formatterTxt);
        logger.addHandler(fileTxt);

        java.util.logging.Logger rootLogger = java.util.logging.Logger.getLogger("");
        Handler[] handlers = rootLogger.getHandlers();
        if (handlers[0] instanceof ConsoleHandler) {
            handlers[0].setFormatter(formatterTxt);
        }

        // create an HTML formatter
        formatterHTML = new HtmlLogFormatter();
        fileHTML.setFormatter(formatterHTML);
        logger.addHandler(fileHTML);

    }
}
