package org.esinecan.asyncsender.utility.readers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static org.esinecan.asyncsender.utility.loggers.Logger.logFileReadError;

/**
 * Decoupled reader for recipients
 *
 * Created by eren.sinecan on 01/05/2017.
 */
public class RecipientInfoReader {
    private Scanner scanner = null;

    /**
     *
     *
     * @param filePath
     */
    public RecipientInfoReader(String filePath){
        File inputF = new File(filePath);
        try {
            scanner = new Scanner(inputF);
        } catch (FileNotFoundException e) {
            logFileReadError(filePath, e);
        }
        scanner.useDelimiter(",");
    }

    public boolean hasNext() {
        return scanner.hasNext();
    }

    public String next() {
        return scanner.next();
    }
}
