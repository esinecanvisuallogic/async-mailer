package org.esinecan.asyncsender;

import io.reactivex.Single;
import org.esinecan.asyncsender.model.Recipient;
import org.esinecan.asyncsender.service.MailSender;
import org.esinecan.asyncsender.utility.readers.RecipientInfoReader;
import org.esinecan.asyncsender.validators.Validator;

import static org.esinecan.asyncsender.utility.loggers.Logger.logSendMailError;

/**
 * * This is separated from main so that we can easily add a
 * @Service annotation on top and use it in a web app.
 *
 * Created by eren.sinecan on 29/04/2017.
 */
public class AsyncSender {

    /**
     * sends mass e-mails specified in the recipientInfoSource
     *
     * @param recipientInfoSource location of the recipient info
     */
    public void send(String recipientInfoSource) {
        RecipientInfoReader reader = new RecipientInfoReader(recipientInfoSource);

        while (reader.hasNext()) {
            final String nextVal = reader.next();

            Single.create(singleEmitter -> {

                Thread thread = new Thread(() -> {
                    validateAndSendMail(nextVal);
                });
                thread.start();

            }).subscribe().dispose(); //We're not interested with the outcome, so we immediately subscribe and dispose.
        }
    }

    /**
     * Runs the validator on chunk, if it's valid, parses it to a Recipient object
     * and sends the mail.
     *
     * @param chunk a portion of text separated by "," signs
     */
    private void validateAndSendMail(String chunk) {
        if(Validator.isValidRecipient(chunk)){

            MailSender mailSender = new MailSender();
            Recipient recipient = Recipient.build(chunk);
            try {
                mailSender.sendMail(recipient);
            } catch (Exception e) {
                logSendMailError(recipient);
            }

        }
    }
}
