package org.esinecan.asyncsender.service;

import org.esinecan.asyncsender.model.Recipient;

import java.util.Random;

import static org.esinecan.asyncsender.utility.loggers.Logger.logSuccess;

/**
 * Mock implementation of a mail sender class
 *
 * Created by eren.sinecan on 01/05/2017.
 */
public class MailSender {

    /**
     * Mocks a real mail sending service with a 0.5 second wait time and %2 chance of failure.
     *
     * @param recipient object containing name and address of recipient
     * @throws Exception
     */
   public void sendMail(Recipient recipient) throws Exception {
       Thread.sleep(500); //We wait for 500 ms as per specs

       //Let's have a chance of failure
       int diceRoll = new Random().nextInt(50);
       if(diceRoll == 2){
           throw new Exception();
       }else{
           logSuccess(recipient);
       }
   }
}
