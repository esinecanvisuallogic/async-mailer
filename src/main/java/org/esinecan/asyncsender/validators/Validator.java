package org.esinecan.asyncsender.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.esinecan.asyncsender.utility.loggers.Logger.logParsingError;

/**
 * Created by eren.sinecan on 29/04/2017.
 */
public class Validator {

    private static final String recipient_PATTERN = "^[\"][_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*[@]" +
            "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})[\"][;]" +
            /** Up to here, we confirmed that the first part of our string is in the form of:
             * "recipient@some-provider.com";
             * if we want to ensure that only a certain company domain should be mailed,
             * we need to edit the line above this comment. */
            "[\"][A-Za-z0-9]*[\"][;][\"][A-Za-z0-9\\s]*[\"]$";
            /** The rest confirms that we have a string like:
             * "firstname0";"von lastname0"*/

    /**
     * Confirms whether a String is of the form:
     * "\"recipient@some-provider.com\";\"firstname0\";\"von lastname0\","
     *
     * @param recipientString a CSV file chunk
     * @return Whether the string is valid or not.
     */
    public static boolean isValidRecipient(String recipientString){
        Pattern pattern = Pattern.compile(recipient_PATTERN);
        Matcher matcher = pattern.matcher(recipientString);
        boolean match = matcher.matches();
        if(!match){
            logParsingError(recipientString);
        }
        return match;
    }
}
