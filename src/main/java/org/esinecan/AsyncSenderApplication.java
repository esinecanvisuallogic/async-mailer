package org.esinecan;

import org.esinecan.asyncsender.AsyncSender;
import org.esinecan.asyncsender.utility.loggers.Logger;

import java.io.IOException;

/**
 * Created by eren.sinecan on 29/04/2017.
 */
public class AsyncSenderApplication {

    public static void main(String[] args) {
        try {
            Logger.setup();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Problems with creating the log files");
        }
        AsyncSender asyncSender = new AsyncSender();
        String filePath = args[0];
        asyncSender.send(filePath);
    }
}
