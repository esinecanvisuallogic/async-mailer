import org.junit.Test;

import static org.esinecan.asyncsender.validators.Validator.isValidRecipient;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by eren.sinecan on 29/04/2017.
 */
public class ValidatorUnitTest {

    @Test
    public void testValidrecipientString(){
        assertTrue(isValidRecipient("\"recipient@some-provider.com\";\"firstname0\";\"von lastname0\""));
    }

    @Test
    public void testInvalidEmailrecipientString(){
        assertFalse(isValidRecipient("\"recipient@some-provider\";\"firstname0\";\"von lastname0\""));
    }

    @Test
    public void testRidiculouslyInvalidrecipientString(){
        assertFalse(isValidRecipient("I like doggies"));
    }
}
