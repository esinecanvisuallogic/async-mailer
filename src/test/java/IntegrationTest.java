import org.esinecan.asyncsender.AsyncSender;
import org.esinecan.asyncsender.utility.loggers.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.*;
import java.net.URL;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by eren.sinecan.
 */
public class IntegrationTest {

    @BeforeClass
    public static void cleanBefore(){
        cleanUp();
    }

    @Test
    public void testApp() throws FileNotFoundException, InterruptedException {
        BufferedReader br = runForFile("test.txt", "Logging.txt");

        assertTrue(br.lines().anyMatch(s -> s.contains("Mail successfully sent to")));
        assertEquals(br.lines().count(), 1998); //Each recipient drops two lines of log, we have 999 of them
    }

    @Test
    public void testApp_Corrupt() throws FileNotFoundException, InterruptedException {
        BufferedReader br = runForFile("testCorrupt.txt", "Logging.txt.1"); //Some recipients are corrupt

        //Testing for 3 and 4
        assertTrue(br.lines().anyMatch(
                s -> s.contains("SEVERE: Parsing failed for the chunk: \"recipient@some-provider.com\";" +
                        "\"firstname3\";\"von lastname3\"\"recipient@some-provider.com\";\"" +
                        "firstname4\";\"von lastname4\"")));

        //Testing for 12
        assertTrue(br.lines().anyMatch(
                s -> s.contains("SEVERE: Parsing failed for the chunk: \"recipient@some-provider.com\";" +
                        "firstname12\";\"von lastname12\"")));
    }

    public BufferedReader runForFile(String fileName, String txtFile) throws InterruptedException, FileNotFoundException {
        try {
            Logger.setup();
        } catch (IOException e) {
            e.printStackTrace();
        }
        URL resource = IntegrationTest.class.getResource(fileName);
        AsyncSender asyncSender = new AsyncSender();
        asyncSender.send(resource.getPath());

        Thread.sleep(15000); //let's give some time for all the lines to finish

        File inputF = new File(txtFile);
        InputStream inputFS = new FileInputStream(inputF);
        return new BufferedReader(new InputStreamReader(inputFS));
    }

    @AfterClass
    public static void cleanUp(){
        cleanUpHandler("Logging.html", "Logging.txt");
        cleanUpHandler("Logging.html.1", "Logging.txt.1");
    }

    public static void cleanUpHandler(String htmlFile, String txtFile){
        try{
            File inputF = new File(txtFile);
            File inputHtml = new File(htmlFile);
            inputF.delete();
            inputHtml.delete();
        }catch (Exception e){

        }
    }
}
