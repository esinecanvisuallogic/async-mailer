# Async Mailer #

Very small mock app of a mail sender that reads the recipients from a text file and sends an e-mail to each of them asynchronously.

### How to run ###

* Run 
```
#!Java

mvn package
```

* Then Run 
```
#!Java

mvn exec:java -Dexec.mainClass="org.esinecan.AsyncSenderApplication" -Dexec.args="/**File Location (i.e. '/Users/eren.sinecan/async-mailer/src/main/resources/test.txt') */"
```

* Quick test: Run
```
#!Java
 mvn test
```